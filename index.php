<?
    require_once('blog.class.php');

    $user1 = new User("Петя");
    $user2 = new User("Вася");
    $manager = new Manager("Менеджер");
    $administrator = new Admin("Администратор");

    $message1 = new Message("Сообщение от Пети", $user1);
    $message2 = new Message("Сообщение от Васи", $user2);

    // вывод сообщений
    echo $message1->getText();
    echo "\r\n";
    echo $message2->getText();
    echo "\r\n";

    // права доступа User на редактирование
    $check1 = $user1->canEdit($message1);
    $check2 = $user1->canEdit($message2);
    $check3 = $user2->canEdit($message2);

    // права менеджера и администратора на редактирование
    $check4 = $manager->canEdit($message1);
    $check5 = $administrator->canEdit($message2);

    // права User Manager и Admin на удаление
    $del1 = $user1->canDelete($message1);
    $del2 = $manager->canDelete($message2);
    $del3 = $administrator->canDelete($message1);

    var_dump($check1, $check2, $check3, $check4, $check5, $del1, $del2, $del3);
?>