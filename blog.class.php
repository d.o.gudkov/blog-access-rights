<?

    /**
    * Пользователь
    * Может редактировать только свои сообщения
    * Удалять сообщения не может
    * Class User
    */
    class User
    {
        /** @var int */
        private $id;

        /** @var string */
        private $name;

        function __construct($name)
        {
            $this->id = random_int(1,100);
            $this->name = $name;
        }
        public function getUserName()
        {
            return $this->name;
        }
        public function getUserID()
        {
            return $this->id;
        }
        /**
        * Проверяет, может ли пользователь редактировать сообщение
        * @param Message $message
        * @return bool
        */
        public function canEdit(Message $message)
        {
            if ($this->id === $message->getUserID()) {
                return true;
            }
            return false;
        }

        /**
        * Проверяет, может ли пользователь удалить сообщение
        * @param Message $message
        * @return bool
        */
        public function canDelete(Message $message)
        {
            return false;
        }
    }

    /**
    * Менеджер
    * Может редактировать свои сообщения и собщения других пользователей
    * Удалять сообщения не может
    * Class Manager
    */
    class Manager extends User
    {
        public function canEdit(Message $message)
        {
            return true;
        }
    }

    /**
    * Администратор
    * Может редактировать и удалять любые сообщения
    * Class Admin
    */
    class Admin extends Manager
    {
        public function canDelete(Message $message)
        {
            return true;
        }
    }

    class Message
    {
        private $id;
        private $date;

        private $text;
        private $user;

        function __construct ($text, User $user)
        {
            $this->text = $text;
            $this->user = $user;
            $this->date = date("d.m.Y H:i:s");
        }

        public function getText()
        {
            return "{$this->user->getUserName()} [{$this->date}]: {$this->text}";
        }
        public function getUserID()
        {
            return $this->user->getUserID();
        }
    }

?>